firewall {
    all-ping enable
    broadcast-ping disable
    group {
        address-group LAN_printer {
            description 192.168.100.100
        }
        network-group Print_Mgmt_Net {
            description "Networks authorized to manage printer"
            network 128.173.96.0/24
            network 128.173.97.0/24
            network 128.173.100.0/22
        }
        network-group Print_Monitor_Net {
            description "Networks authorized to monitor printer"
            network 128.173.100.0/22
        }
        network-group Print_Service_Net {
            description "Networks authorized to print"
            network 128.173.0.0/16
            network 198.82.0.0/16
            network 172.16.0.0/12
            network 45.3.64.0/20
        }
        network-group Router_Mgmt_Net {
            description "Networks authorized to manage router"
            network 128.173.100.0/22
        }
        network-group Router_Monitor_Net {
            description "Networks authorized to monitor the router"
            network 128.173.97.0/24
        }
        port-group Print_Mgmt_Ports {
            description "Ports for printer management"
            port 80
            port 443
        }
        port-group Print_Monitor_Ports {
            description "Ports for printer monitoring"
            port 161
            port 80
            port 443
        }
        port-group Print_Service_Ports {
            description "Ports for print services"
            port 515
            port 631
            port 9100
        }
        port-group Router_Mgmt_Ports {
            description "Ports for router management"
            port 8080
            port 8443
            port 22
        }
        port-group Router_Monitor_Ports {
            description "Ports for router monitoring"
            port 8161
        }
    }
    ipv6-receive-redirects disable
    ipv6-src-route disable
    ip-src-route disable
    log-martians enable
    name WAN_IN {
        default-action drop
        description "WAN to internal"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
        rule 30 {
            action accept
            description "Print services from Print_Service_Net"
            destination {
                group {
                    port-group Print_Service_Ports
                }
            }
            log disable
            protocol tcp_udp
            source {
                group {
                    network-group Print_Service_Net
                }
            }
        }
        rule 40 {
            action accept
            description "Printer management from Print_Mgmt_Net"
            destination {
                group {
                    port-group Print_Mgmt_Ports
                }
            }
            log disable
            protocol tcp_udp
            source {
                group {
                    network-group Print_Mgmt_Net
                }
            }
        }
        rule 41 {
            action accept
            description "Printer monitoring from Print_Monitor_Net"
            destination {
                group {
                    port-group Print_Monitor_Ports
                }
            }
            log disable
            protocol tcp_udp
            source {
                group {
                    network-group Print_Monitor_Net
                }
            }
        }
    }
    name WAN_LOCAL {
        default-action drop
        description "WAN to router"
        rule 10 {
            action accept
            description "Allow established/related"
            state {
                established enable
                related enable
            }
        }
        rule 20 {
            action drop
            description "Drop invalid state"
            state {
                invalid enable
            }
        }
        rule 21 {
            action accept
            description "ICMP echo"
            log disable
            protocol icmp
            source {
                group {
                    network-group Print_Service_Net
                }
            }
        }
        rule 22 {
            action accept
            description "Router management - Router_Mgmt_Net"
            destination {
                group {
                    port-group Router_Mgmt_Ports
                }
            }
            log enable
            protocol tcp
            source {
                group {
                    network-group Router_Mgmt_Net
                }
            }
        }
    }
    receive-redirects disable
    send-redirects enable
    source-validation disable
    syn-cookies enable
}
interfaces {
    ethernet eth0 {
        address 192.168.100.1/24
        description Local_Printer
        duplex auto
        speed auto
    }
    ethernet eth1 {
        description Local
        duplex auto
        speed auto
    }
    ethernet eth2 {
        description Local
        duplex auto
        speed auto
    }
    ethernet eth3 {
        description Local
        duplex auto
        speed auto
    }
    ethernet eth4 {
        address dhcp
        description Internet
        duplex auto
        firewall {
            in {
                name WAN_IN
            }
            local {
                name WAN_LOCAL
            }
        }
        speed auto
    }
    loopback lo {
    }
    switch switch0 {
        address 192.168.2.1/24
        description Local
        mtu 1500
        switch-port {
            interface eth1 {
            }
            interface eth2 {
            }
            interface eth3 {
            }
            vlan-aware disable
        }
    }
}
service {
    dhcp-server {
        disabled false
        hostfile-update disable
        shared-network-name LAN1 {
            authoritative enable
            subnet 192.168.100.0/24 {
                default-router 192.168.100.1
                dns-server 192.168.100.1
                lease 3600
                start 192.168.100.100 {
                    stop 192.168.100.100
                }
            }
        }
        shared-network-name LAN2 {
            authoritative enable
            subnet 192.168.2.0/24 {
                default-router 192.168.2.1
                dns-server 192.168.2.1
                lease 86400
                start 192.168.2.38 {
                    stop 192.168.2.243
                }
            }
        }
        use-dnsmasq disable
    }
    dns {
        forwarding {
            cache-size 150
            listen-on eth0
            listen-on switch0
        }
    }
    gui {
        http-port 80
        https-port 8443
        older-ciphers enable
    }
    nat {
        rule 2 {
            description "NAT - forward Print_Mgmt_Ports to Printer"
            destination {
                group {
                    port-group Print_Mgmt_Ports
                }
            }
            inbound-interface eth4
            inside-address {
                address 192.168.100.100
            }
            log enable
            protocol tcp_udp
            type destination
        }
        rule 3 {
            description "NAT - forward Print_Service_Ports to Printer"
            destination {
                group {
                    port-group Print_Service_Ports
                }
            }
            inbound-interface eth4
            inside-address {
                address 192.168.100.100
            }
            log disable
            protocol tcp_udp
            type destination
        }
        rule 4 {
            description "NAT - forward Print_Monitor_Ports to Printer"
            destination {
                group {
                    port-group Print_Monitor_Ports
                }
            }
            inbound-interface eth4
            inside-address {
                address 192.168.100.100
            }
            log enable
            protocol tcp_udp
            type destination
        }
        rule 5010 {
            description "masquerade for WAN"
            outbound-interface eth4
            type masquerade
        }
    }
    ssh {
        port 22
        protocol-version v2
    }
}
system {
    host-name edgerouterx
    login {
        user admin {
            authentication {
                encrypted-password $6$inuU5Rcp$EtSizuHCwY2a7ukLp7QQBajcHbkkFyJE9yb.NauIafw.4t5T.5rX8qOqpUvn1srd2r.Vv6gKBIrAdpxb8E5mE/
                plaintext-password ""
            }
            full-name Administrator
            level admin
        }
    }
    ntp {
        server 0.ubnt.pool.ntp.org {
        }
        server 1.ubnt.pool.ntp.org {
        }
        server 2.ubnt.pool.ntp.org {
        }
        server 3.ubnt.pool.ntp.org {
        }
    }
    syslog {
        global {
            facility all {
                level notice
            }
            facility protocols {
                level debug
            }
        }
    }
    time-zone America/New_York
}


/* Warning: Do not remove the following line. */
/* === vyatta-config-version: "config-management@1:conntrack@1:cron@1:dhcp-relay@1:dhcp-server@4:firewall@5:ipsec@5:nat@3:qos@1:quagga@2:system@4:ubnt-pptp@1:ubnt-util@1:vrrp@1:webgui@1:webproxy@1:zone-policy@1" === */
/* Release version: v1.9.1.4939092.161214.0702 */
