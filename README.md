# Printer firewall using EdgeRouterX

Quick links:

* [configuration tarball](config/edgeos_edgerouterx_20200204_template.tar.gz)
* [plain text config](config/config.boot)


## Goal

Goal: implement a firewall for printers without built-in one.

Internet connected printers are very common, yet many lack proper firewalls or even basic access control lists for its services. This project documents a simple setup using an inexpensive EdgeRouterX running EdgeOS to quickly create a firewall that fits in our environment.

We want to go from:



### Before

![before](graphs/diagram_before.png)

### After

![after](graphs/diagram_after.png)

### And to be more specific



![After, in detail](graphs/diagram_after_really.png)



### Initial Requirements:

- Inexpensive hardware, ideally less than $50.
- Two or more, network interfaces. One for WAN access, other for the printer.
- Ability to load preset configuration, unit agnostic.
- Printer agnostic: can be placed in front of any network printer, without customizing MAC address, port numbers or interfering with existing printer management UI.
- Ease of management, printer ports or allowable network ranges should be defined in a single location.
- Remote management.



### Requirement details

How are we trying to meet the requirements?

* Inexpensive hardware, ideally less than $50. 
  * EdgeRouterX is currently available for roughly $50 with S&H.
* Two or more, network interfaces. One for WAN access, other for the printer.
  * We really need only two interfaces, but in case if we need to perform configuration changes/diagnostics with a local system, we can use one of the remaining ports on the EdgeRouterX.
* Ability to load preset configuration.
  * EdgeOS provides us ability to configure a device, export its config, and then load it to another. So far other than existing DHCP leases, we get a fairly device agnostic config tarball.
* Printer agnostic: can be placed in front of any network printer, without customizing MAC address, port numbers or interfering with existing printer management UI.
  * By setting up DHCP service on a designated port with a range of a single address, 192.168.100.100, we are making sure that whatever gets plugged into that port will be on the expected IP address. This way we don't have to change configuration as we deploy these systems for different printers.
* Ease of management, printer ports or allowable network ranges should be defined in a single location.
  * EdgeOS is fairly flexible, and provides us ability to create groups of network ranges and/or ports. Then using those groups we can craft NAT and firewall rules. Consequently, changing ports or network ranges require adjusting only the group definition, rather than individual rules.
* Remote management.
  * We're able to manage the devices via web UI and ssh. Unfortunately, there seems to be no simple central management system. 



## Skip the chitchat, let's get it going



### Setup

* Power on the EdgeRouterX, connect ethernet to port labeled **eth0**
* Set your network adapter to 192.168.1.100 and then use a browser to navigate to [http://192.168.1.1](http://192.168.1.1)
* Login with username **ubnt** and password **ubnt**
* Upgrade to the latest firmware
  * Download it from [https://www.ubnt.com/download/#!edgemax](https://www.ubnt.com/download/#!edgemax) (latest, at the time of writing this, is [1.9.1](http://dl.ubnt.com/firmwares/edgemax/v1.9.1/ER-e50.v1.9.1.4939092.tar))
  * Select System->Upgrade System Image (from the bottom left of the web UI)
* Upload configuration
  * Select System->Restore Config
  * Use the provided [configuration tarball](config/edgeos_edgerouterx_20170209_template.tar.gz)
* Move ethernet cable for management from **eth0** to **eth1**
* Connect your printer to **eth0**
* Connect **eth04** interface to WAN
  * By default we set it to get an IP via DHCP from your WAN


At this point you have a device that will get an IP from DHCP on WAN, and will forward various printing services to the local printer. Administrative UI is available when connecting to either

* **https://192.168.2.1:8443** from ports **eth1-eth3**, or
* ssh admin@192.168.2.1from ports **eth1-eth3**, or
* https://WAN_IP:8443 from a network range defined under Router_Mgmt_Net, or
* ssh admin@WAN_IP  from a network range defined under Router_Mgmt_Net



Default admin user is **admin** and password is **printer**.



### Configuration

At this point you have a device that will get an IP from DHCP on WAN, and will forward various printing services to the local printer. It's important you change the default password for the admin user as soon as possible.



Initial username & password, should be changed as soon as initial configuration is imported:

URL: [https://192.168.2.1:8443](https://192.168.2.1:8443)

Username: **admin**

Password: **printer**





## Now, how does it work?



Clear as mud diagram of what happens inside:

![Basic overview](graphs/diagram_basic_setup.png)

* Network printer is attached to **eth0** interface (labeled **IN** on the router), where it receives an internal IP
* Internet connectivity is attached to **eth4** (labeled **OUT**), where it receives external IP. Typically this would be the previous IP of the printer, this way clients do not have to be reconfigured.
* Networks defined in **Print_Service_Net** group can send print jobs to the external IP







## Firewall & NAT

EdgeOS has a notion of NAT and two firewalls:

* WAN_IN
  * This governs packets that will be routed elsewhere
* WAN_LOCAL
  * This manages packets destined for EdgeRouterX itself



![Basics of firewall & NAT](graphs/diagram_firewall_nat_overview.png)

With the aid of groups we can then apply rules for NAT & WAN_IN/WAN_LOCAL firewalls:



![Network and port groups](graphs/diagram_groups.png)



EdgeOS is based on Linux, and consequently it relies on its network framework. For port forwarding to printer, we have to define rules on both firewall and NAT components. Thankfully, EdgeOS allows us to abstract network and port groups, so we can define our customer networks in one place, and use those for both firewall and NAT rulesets.



Here is a list of network and port groups and a brief description of what we use them for:

### Network groups:

* Print_Service_Net
  * IPs/networks allowed to print.
* Print_Mgmt_Net
  * IPs/networks allowed reach printer web UI.
* Print_Monitor_Net
  * IPs/networks which can monitor printers via SNMP.
* Router_Mgmt_Net
  * Ps/networks allowed to access EdgeOS web UI.
* Router_Monitor_Net
  * IPs/networks which can monitor EdgeOS via SNMP.



### Port groups:

* Print_Service_Ports
  * Ports for print services, eg: IPP, LPR, JetDirect.
* Print_Mgmt_Ports
  * Ports for managing the printers, typically HTTP and HTTPS.
* Print_Monitor_Ports
  * SNMP and possible other ports for monitoring printer status.
* Router_Mgmt_Ports
  * Ports for managing the EdgeOS device. We reconfigure the devices to listen on **8443**.
* Router_Monitor_Ports
  * Ports for SNMP monitoring of the EdgeOS device, which we reconfigure to use 8161.




## Everything works, now I want to...



### Change what networks can access the printers

Navigate to:

* Firewall/NAT on top menu
  * Firewall/NAT groups
  * Select 



## Random notes

* To configure remote logging, select System-> System Log -> Log to remote server
  * Logs don't seem to have much interesting/useful data, even when set to the highest level
* Time zone in this configuration is set to EST. To change it
  * System -> Time Zone
* Host name is set to **edgerouterx**, to change it:
  * System -> Host Name
* DHCP lease time for the printer port is set to **1 hour**
* EdgeOS runs Debian linux
* User passwords are hashed with SHA512
* **Print_Monitor_Ports** include SNMP, which is pretty much required to query availability & supply level by many drivers

### Wishlist / todo list

* Create separate ruleset for printer discovery ports and networks authorized to acces them.
* Identify an easier, maybe even automated, method for initial configuration of the edge router X devices.
* Identify a method, if possible, to easily deploy configuration changes for multiple edge router X devices.
* Identify a method, if possible, to automate firmware upgrades for multiple edge router X devices.
* Split TCP and UDP ports into separate groups and rules
* Add the missing Print_Monitor_Ports to the diagrams